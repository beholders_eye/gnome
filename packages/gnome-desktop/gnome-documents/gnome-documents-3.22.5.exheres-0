# Copyright 2012-2013 Marc-Antoine Perennou<Marc-Antoine@Perennou.com
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings
require gtk-icon-cache freedesktop-desktop

SUMMARY="A document manager application for GNOME"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( linguas:
        af ar as be bg bn_IN bs ca ca@valencia cs da de el en_GB eo es et eu fa fi fr fur ga gl gu
        he hi hu id it ja kk kn ko ln lt lv mk ml mr nb ne nl oc or pa pl pt pt_BR ro ru sk sl sr
        sr@latin sv ta te tg th tr ug uk vi xh zh_CN zh_HK zh_TW
    )
"
HOMEPAGE="https://wiki.gnome.org/Apps/Documents"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        dev-util/intltool[>=0.50.1]
        gnome-desktop/yelp-tools
        sys-devel/gettext
        sys-devel/libtool
        virtual/pkg-config
    build+run:
        app-pim/tracker:1.0[>=0.17.3][gobject-introspection]
        dev-libs/glib:2[>=2.39.3]
        dev-libs/libgepub[>=0.4][gobject-introspection]
        dev-libs/libzapojit[>=0.0.2][gobject-introspection]
        gnome-bindings/gjs:1
        gnome-desktop/evince[>=3.13.3][gobject-introspection]
        gnome-desktop/gnome-desktop:3.0[gobject-introspection]
        gnome-desktop/gnome-online-accounts[>=3.2.0][gobject-introspection]
        gnome-desktop/gobject-introspection:1[>=1.31.6]
        gnome-desktop/libgdata[>=0.13.3][gobject-introspection][gnome]
        gnome-desktop/libsoup:2.4[>=2.41.3]
        net-libs/webkit:4.0[>=2.6.0][gobject-introspection]
        x11-libs/gdk-pixbuf:2.0[gobject-introspection]
        x11-libs/gtk+:3[>=3.19.1][gobject-introspection]
        x11-libs/pango[gobject-introspection]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --prefix=/usr
    --exec_prefix=/usr/$(exhost --target)
)

pkg_postinst() {
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
}

pkg_postrm() {
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
}

