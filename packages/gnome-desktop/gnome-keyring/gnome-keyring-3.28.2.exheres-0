# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome-keyring

LICENCES="GPL-2"
SLOT="1"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="caps doc pam
    caps [[
        description = [ Enable Linux capabilities to prevent the keyring's memory from being paged to swap ]
    ]]
    pam [[ description = [ Install a PAM module to unlock the keyring on login ] ]]
    ssh-agent [[ description = [ Disable this to use the one from openssh or gnupg ] ]]
    ( linguas: af ar as ast az be be@latin bg bn bn_IN bs ca ca@valencia cs cy da de dz el en_CA
               en_GB en@shaw eo es et eu fa fi fr ga gl gu he hi hr hu id is it ja ka km kn ko lt
               lv mai mg mk ml mn mr ms nb ne nl nn oc or pa pl pt pt_BR ro ru rw si sk sl sq sr
               sr@latin sv ta te tg th tr ug uk vi xh zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.18]
        doc? ( dev-libs/libxslt )
    build+run:
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libgcrypt[>=1.2.2]
        gnome-desktop/gcr[>=3.27.90][gobject-introspection]
        caps? ( sys-libs/libcap-ng )
        pam? ( sys-libs/pam )
"

RESTRICT="test" # requires X

src_configure() {
    econf '--disable-schemas-compile'                                           \
          '--disable-selinux'                                                   \
          '--disable-p11-tests'                                                 \
          $(expecting_tests && echo '--enable-tests' || echo '--disable-tests') \
          $(option_enable doc)                                                  \
          $(option_with caps libcap-ng)                                         \
          $(option_enable pam)                                                  \
          $(option_with pam pam-dir "/usr/$(exhost --target)/lib/security")     \
          $(option_enable ssh-agent)
}

