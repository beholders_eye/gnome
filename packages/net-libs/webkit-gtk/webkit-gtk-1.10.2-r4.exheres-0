# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require webkit-gtk

SLOT="1"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="debug [[ description = [ debug build of webkit (not recommended for daily usage) ] ]]
doc
gobject-introspection
media [[ description = [ HTML5 video/audio support ] ]]
spell
opengl [[ description = [ enable accelerated rendering via OpenGL (enables WebGL) ] ]]
( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/ruby:*
        dev-lang/perl:*
        dev-lang/python:*
        sys-devel/bison
        sys-devel/flex[>=2.5.33] [[ note = [ add a dependency on a system package for the version ] ]]
        dev-util/gperf
        virtual/pkg-config[>=0.20]
        doc? ( dev-doc/gtk-doc[>=1.10] )
    test:
        dev-libs/at-spi2-core[>=2.2.1]
    build+run:
        media-libs/libpng:=[>=1.2]
        dev-libs/glib:2[>=2.32.0]
        dev-libs/icu:= [[ note = [ remove when glib unicode backend is recommended ] ]]
        sys-libs/zlib
        dev-libs/libxml2:2.0[>=2.6]
        x11-libs/pango[>=1.21.0]
        x11-libs/cairo[>=1.10]
        x11-libs/libXt
        gnome-desktop/libgudev
        gnome-desktop/libsoup:2.4[>=2.39.2][gobject-introspection?]
        media-libs/fontconfig[>=2.4]
        media-libs/freetype:2
        dev-db/sqlite:3[>=3.0]
        dev-libs/libxslt[>=1.1.7]
        x11-libs/libXrender
        x11-dri/mesa
        x11-libs/gtk+:2[>=2.10][gobject-introspection?]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.5] )
        media? ( media-libs/gstreamer:1.0[>=0.11.90]
                 media-plugins/gst-plugins-base:1.0[>=0.11.90] )
        spell? ( app-spell/enchant:0[>=0.22] )
        opengl? ( x11-dri/mesa
                  x11-libs/libXcomposite
                  x11-libs/libXdamage )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    -p0 "${FILES}"/${PNV}-bison-3.patch
)

RESTRICT="test" # requires X

src_configure() {
    local additional_gtk_features=

    # NOTE (abdulras) this list is based upon the feature list from FeatureList.pm from webkitperl
    # It enables functionality which continuous integration builds contain but the autotools based
    # build does not.  See: Tools/Scripts/webkitperl/FeatureList.pm
    #
    # --enable-media-stream and --enable-video-track are wrapped with [media] since we
    # do not unconditionally depend on gstreamer
    additional_gtk_features="--enable-gamepad --enable-link-prefetch --enable-web-timing"

    econf '--with-gtk=2.0'                                              \
          '--with-gstreamer=1.0'                                        \
          '--disable-geolocation'                                       \
          '--disable-webkit2'                                           \
          '--with-font-backend=freetype'                                \
          $(option_with opengl acceleration-backend opengl)             \
          $(option_enable opengl webgl)                                 \
          $(option_enable opengl accelerated-compositing)               \
          $(option_enable debug debug-features)                         \
          $(option_enable debug debug-symbols)                          \
          $(option_enable doc gtk-doc)                                  \
          $(option_enable gobject-introspection introspection)          \
          $(option_enable media video)                                  \
          $(option_enable media video-track)                            \
          ${additional_gtk_features}
}

